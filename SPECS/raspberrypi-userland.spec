%global rpi_userland_ver 0.0.1
%global userland_commit cc1ca18fb0689b01cc2ca2aa4b400dcee624a213
%global userland_url https://github.com/raspberrypi/userland
%global target_lib lib64
%global build_type release
%global build_dir xxx
%global prefix build/raspberry/%{build_type}/%{build_dir}/opt/vc


Name:		raspberrypi-userland
Version:	%{rpi_userland_ver}
Release:	1%{?dist}
Summary:	Raspberry PI userland utilites
License:	BSD 3-Clause License
ExcludeArch:	i386 i686 x86_64 ppc64le s390x
ExclusiveArch:	aarch64

URL:		%{userland_url}
Source0:	%{userland_url}/archive/%{userland_commit}/userland-%{userland_commit}.tar.gz

Patch0:		fix-sudo-install.patch
Patch1:		fix-buildme-arm64.patch
Patch2:		fix-debuginfo.patch

BuildRequires:	gcc
BuildRequires:	gcc-c++
BuildRequires:	cmake

%description
This package contains some utilities to operate with Raspberry PI
unsing its GPU in order to access some service functions like a
temperature, frequency, and voltage monitoring and so on.

%package devel
Summary: This package contains header files for libraries from %{name} package
Requires: %{name} = %{version}-%{release}
Requires: %{name}-static = %{version}-%{release}

%description devel
This package contains library headers for both static and shared libraries.

%package static
Summary: This package contains static libraries files for %{name} package

%description static
This package contains static libraries.

%prep

%setup -q -n userland-%{userland_commit}

%patch0 -p1
%patch1 -p2
%patch2 -p1

%build

./buildme --force-arm64 %{build_dir}

%install

mkdir -p %{buildroot}/usr/%{target_lib}
mkdir -p %{buildroot}/usr/%{target_lib}/pkgconfig
mkdir -p %{buildroot}/usr/bin
mkdir -p %{buildroot}/usr/share/%{name}
mkdir -p %{buildroot}/usr/include/
mkdir -p %{buildroot}/usr/share/man/man1
mkdir -p %{buildroot}/usr/share/man/man7

%{__install} -m 755 %{prefix}/bin/* %{buildroot}/usr/bin
%{__install} -m 755 %{prefix}/lib/*.so %{buildroot}/usr/%{target_lib}
%{__install} -m 755 %{prefix}/lib/*.a %{buildroot}/usr/%{target_lib}
%{__install} -m 644 %{prefix}/man/man1/*.1 %{buildroot}/usr/share/man/man1
%{__install} -m 644 %{prefix}/man/man7/*.7 %{buildroot}/usr/share/man/man7
cp -r %{prefix}/include/interface %{buildroot}/usr/include
cp -r %{prefix}/include/vcinclude %{buildroot}/usr/include
%{__install} -m 644 %{prefix}/include/bcm_host.h %{buildroot}/usr/include
%{__install} -m 444 ./LICENCE %{buildroot}/usr/share/%{name}

# change prefix=/opt/vc to prefix=/usr and libdir=\${exec_prefix}/lib to libdir=\${exec_prefix}/%{target_lib}
/bin/sh -c "for p in %{prefix}/lib/pkgconfig/*.pc; \
  do sed 's/prefix=\/opt\/vc/prefix=\/usr/' \$p | sed 's/}\/lib/}\/%{target_lib}/' \
  > %{buildroot}/usr/%{target_lib}/pkgconfig/\$(basename \$p); done"

%files
/usr/%{target_lib}/*.so
/usr/%{target_lib}/pkgconfig/*.pc
/usr/bin/*
/usr/share/%{name}/LICENCE
/usr/share/man/man1/*.1.gz
/usr/share/man/man7/*.7.gz

%files devel
/usr/include/*

%files static
/usr/%{target_lib}/*.a

%changelog
* Fri Aug 11 2023 Pavel Kopylov <pkopylov@cloudlinux.com> - 0.0.1-1
- Initial preparation
